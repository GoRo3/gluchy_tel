#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <asm/uaccess.h>
#include <asm/errno.h>
#include <linux/cdev.h>
#include <linux/kdev_t.h>

#define DEVICE_NAME "liczbaFib"
#define CLASS_NAME "Fibo"

#define MAX_BUFF_SIZE 1024

MODULE_LICENSE("GPL");
MODULE_AUTHOR("GORO3");
MODULE_DESCRIPTION("Modul przeznaczony dla zadania nr 4");
MODULE_VERSION("0.5");


static int majorNumber;
static int numberOpens=0;
static dev_t first_dev;

long int Liczba;
unsigned int liczbaPoOpracowaniu;

static int dev_open(struct inode *, struct file *);
static int dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char  *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static char dev_buffer[MAX_BUFF_SIZE];

static int fibonacci(unsigned long);

static struct device *liczbaFibDevice;
static struct class *liczbaFibClass;
static struct cdev my_cdev;

static struct file_operations fops = {
	.read = dev_read,
	.write = dev_write,
	.open = dev_open,
	.release = dev_release
};

static int __init liczba_init(void){

	majorNumber = alloc_chrdev_region(&first_dev,0,1,DEVICE_NAME);
	if(majorNumber<0)
	{
		printk(KERN_ERR "Liczba_Fib failed to register a mojur number\n.");
		return majorNumber;
	}

	// Register the device class
   	liczbaFibClass = class_create(THIS_MODULE, CLASS_NAME);
   	if (IS_ERR(liczbaFibClass))
	{
      		unregister_chrdev(majorNumber, DEVICE_NAME);
      		printk(KERN_ALERT "Nie udlo sie zarejesrowac Classy obiektu\n");
      		return PTR_ERR(liczbaFibClass);
   	}
   	printk(KERN_INFO "LICZBA_Fib: Poprawnie utworzono klase obiektu\n");

	liczbaFibDevice = device_create(liczbaFibClass, NULL, first_dev, NULL, DEVICE_NAME);
	if (IS_ERR(liczbaFibDevice))
	{
		class_destroy(liczbaFibClass);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Nie udalo sie stworzyc urzadzenia\n");
		return PTR_ERR(liczbaFibDevice);
	}

	cdev_init(&my_cdev,&fops);
	if(cdev_add(&my_cdev,first_dev,1)==-1)
	{
		device_destroy(liczbaFibClass, first_dev);
		class_destroy(liczbaFibClass);
		unregister_chrdev_region(first_dev,1);
		printk(KERN_ALERT "UWAGA! Nie udało się stowrzyć pliku liczbaFib. \n");
		return -1;
	}

	printk(KERN_INFO "Liczba_Fib: Udalo sie stworzyc urzazenie\n"); // Made it! device was initialize


return 0;
}

static void __exit liczba_exit(void)
{
	cdev_del(&my_cdev);
	device_destroy(liczbaFibClass,first_dev);
	class_destroy(liczbaFibClass);
	unregister_chrdev_region(first_dev,1);
	printk(KERN_INFO "Wyrejestrowano chrdev. \n");
}

static int dev_open(struct inode *inode_p, struct file *file_p)
{

	if(numberOpens)
		return -EBUSY;

	numberOpens++;

	printk(KERN_INFO "Liczba_fib: Device hac been opened %d times.\n", numberOpens);
	try_module_get(THIS_MODULE);

	return 0;
}

static int dev_release(struct inode *inode_p, struct file *file_p){
	numberOpens --;
	module_put(THIS_MODULE);

	printk(KERN_INFO "Urzadzenie zostalo zwolnione.\n");
	return 0;
}
static ssize_t dev_write(struct file * file_p, const char *buff, size_t len, loff_t *off){

	size_t buff_len = len;
	if (buff_len > MAX_BUFF_SIZE)
	{
		buff_len = MAX_BUFF_SIZE;
	}
	if(copy_from_user(dev_buffer,buff,buff_len))
	{
		return -EFAULT;
	}

	kstrtol(dev_buffer,10,&Liczba);

	printk(KERN_INFO "Liczba przekazana to: %lu \n",Liczba);

	liczbaPoOpracowaniu = fibonacci(Liczba);
	if(!liczbaPoOpracowaniu){

		printk(KERN_ERR "Blad przy tworzeniu liczby fibbonaciego");
		return -1;
	}
	return 0;
}
static ssize_t dev_read(struct file *file_P, char *buffer, size_t lenght, loff_t *offset)
{

	char buff[99]={0};

	sprintf(buff,"%u",liczbaPoOpracowaniu);
	printk(KERN_INFO "liczba po opracowaniu %u \n",liczbaPoOpracowaniu);
	copy_to_user(buffer,buff,sizeof(buff));

return 0;
}

static int fibonacci(unsigned long liczba){

	unsigned int fib1 = 0;
	unsigned int fib2 = 1;
	unsigned int temp = 0;
	unsigned int i, n = 4294967295;
	for(i=3; i<=n; i++){
		if(temp >= (liczba))
		{
			return temp;
		}
	temp = fib1 + fib2;
	fib1 = fib2;
	fib2 = temp;
	}

return 0;
}

module_init(liczba_init);
module_exit(liczba_exit);
