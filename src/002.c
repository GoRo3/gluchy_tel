
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define FIFO_ADDR "/tmp/fifo_pipe"

unsigned int odbicie_lustrzane(unsigned int);
int sendToFifo(const char *,unsigned int);

// główna funkcja programu
int main(int argc, char **argv)
{
	pid_t pid;
	char *fifo = FIFO_ADDR;

	mkfifo(fifo, 0666);

	// zmienne pomocnicze
	int c;
	unsigned int liczba, odbicie;

	// próba parsowania argumentu, program przyjmuje jeden argument liczbowy i można go wywołać np. tak:
	//
	// ./002.out -n 1234
	//
	// gdzie przekazanym argumentem jest liczba 1234, a nazwą programu 002.out
	while ((c = getopt(argc, argv, "n:")) != -1)
	{
		// jeżeli przekazano argument -n
		if (c == 'n')
		{
		pid = fork();
		if(pid == 0)
		{
			execlp("./build/prog_3","./build/prog_3",NULL);
			perror("...Blad uruchmienia programu\n");
                        exit(1);
		}
			// pobranie argumentu (przekazanej liczby)
			liczba = atoi(optarg);
			printf("Prog 2: Otrzymalem argument z lini polecen (PIPE): %u \n",liczba);
			// odbicie liczby w lustrze
			odbicie = odbicie_lustrzane(liczba);
			printf("Prog 2: Wysyłam do named PIPE (FIFO) liczbe: %u \n",odbicie);
			sendToFifo(fifo,odbicie);
			wait(NULL);
		}
	}

	// koniec programu
	return 0;
}

int sendToFifo(const char *fifo, unsigned int liczba)
{
	char buff[1024] ={};

	int fd = open(fifo,O_WRONLY);
	snprintf(buff,sizeof(buff),"%i",liczba);
	write(fd,buff,strlen(buff)+1);

	close(fd);

	return 0;
}

unsigned int odbicie_lustrzane(unsigned int n)
{
        // zmienne pomocnicze
        unsigned int bit, rezultat;
        int i;

        // wstępne wyzerowanie rezultatu
        rezultat = 0;

        // pętla po wszystkich bitach liczby
    for (i = 0; i < 32; ++i) 
    {
        // pobranie kolejnego bitu
        bit = n & 0x01;
        // wpisanie bitu do rezultatu
        rezultat |= bit;
        // przesunięcie bitów liczby wejściowej w prawo
        n >>= 1;
        // przesunięcie bitów rezultatu w lewo
        rezultat <<= 1;
    }

    // zwrócenie rezultatu
    return rezultat;
}
