#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int bezwzgledna (int liczba)
	{
	if (liczba<0)
		return liczba*(-1);
	else
		return liczba;
	}

int main()
	{
	unsigned int n, pom;
	for(;;)
		{
		printf("\nProg_1: Podaj liczbę całkowitą: ");
		scanf("%d", &n);
		printf("|%d| = %d\n",n,bezwzgledna(n));

		pom=bezwzgledna(n)+1;
		printf("|%d| + 1 = %d\n",n,pom);

		if (fork() == 0)
		{
			char buff[1024];
			snprintf(buff, sizeof(buff),"%d",pom);
			execlp("./build/prog_2","./build/prog_2","-n",buff,NULL);
			perror("Prog_1: Blad uruchmienia programu");
			exit(1);
		}
		wait(NULL);
		printf("\nProg_1:  Koniec pętpli - dziecko się zamknęło i pętla zacznie się ponownie.\n");
		// tak powinno siue kończyć forka , ale nasz program docelowo skończy się inaczej,
		// tzn sam się zabije jak nie będzie widział PIDa rodzica,
		// tworzenie pliku i zapisanie tam pida będzie wpisany do tego programu nieco później 
		// pierwotnie chciałem po xzakończeniu prac Michała, 
		// ale zapewne dzisiaj i jutro będę nad tym pracował (śr i czw). 
		}
	return 0;
	}


