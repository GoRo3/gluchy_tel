#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

#define FIFO_ADDR "/tmp/fifo_pipe"
#define CHRDEV_ADDR "/dev/liczbaFib"

unsigned int readFIFO();
unsigned int writeChrDev(unsigned int);
unsigned int readChrDev();
unsigned int liczbaPierwsza(unsigned int);

int main(void){

	unsigned int  temp=0;

		temp =0;
		temp = readFIFO();
		temp = liczbaPierwsza(temp);
		writeChrDev(temp);
		printf("Prog 3/4: ChrDev file fibonacci = %u \n", readChrDev());
		printf("\n*** Koniec algorytmu gluchy telefon. Nacisznij jakis klawisz... *** \n");
		getchar();
return 0;
}

unsigned int readFIFO(){

	char *fifo = FIFO_ADDR;
	unsigned int liczba = 0;
	char readbuff[99];

	int fd = open(fifo,O_RDONLY);
	printf("Prog_3: Czytam dane wyslane przez Named PIPE (FIFO).\n");
	read(fd,&readbuff,sizeof(readbuff));
	close(fd);
	liczba=atoi(readbuff);
	printf("Prog_3: Odczytana liczba to: %u .\n",liczba);
	return liczba;
}
unsigned int writeChrDev(unsigned int liczba){
	int fd,err;
	char buff[99]={0};

	sprintf(buff,"%u",liczba);
	fd = open(CHRDEV_ADDR,O_WRONLY);
	if(fd<0)
	{
		perror("Prog_3: Nie udalo sie otworzyc pliku urzadzenia: \n");
		exit(EXIT_FAILURE);
	}
	err = write(fd,buff,sizeof(buff));
	if(err<0)
	{
		perror("Prog _3: NIe udalo sie zapisac do pliku urzadzenia: \n");
		close(fd);
		exit(EXIT_FAILURE);
	}
	close(fd);

return 0;
}
unsigned int readChrDev(){

        int fd,err;
	unsigned int liczba =0;
	char buff[99]={0};

        fd = open(CHRDEV_ADDR,O_RDONLY);
        if(fd<0)
        {
                perror("Prog_3: Nie udalo sie otworzyc pliku urzadzenia: \n");
		exit(EXIT_FAILURE);
        }
        err = read(fd,buff,sizeof(buff));
        if(err<0)
        {
                perror("Prog_3: NIe udalo sie zapisac do pliku urzadzenia: \n");
        	close(fd);
		exit(EXIT_FAILURE);
	}
        close(fd);
	liczba = atoi(buff);

return liczba;
}

unsigned int liczbaPierwsza(unsigned int liczba)
{
	unsigned long liczbaPierwsza;
	int i=2;

	while(1)
	{
		if(liczbaPierwsza >= liczba)
		{
		liczba = (unsigned int)liczbaPierwsza;
		break;
		}
		else if(liczbaPierwsza >= 4294967295)
		{
		liczba = 0;
		printf("Prog_3: NIe udalo sie znalezc liczby pierwszej w zakresie u_int !! \n");
		return 1;
		}
	liczbaPierwsza = liczba%i;
	i++;
	}
printf("Prog 3: Najblisza nastepna liczba pierwsza po opracowaniu to: %u \n",liczba);

return liczba;
}
