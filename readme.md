## 		Złożenie projektu

Napisać serie programów komunikujących się ze sobą za pomocą różnych protokołów IPC. 
Każdy z programów ma za zadanie przed przekazaniem wiadomości przekształcić ją w odpowiedni sposób.

Wiadomość: liczba unsigned 32 bity Środowisko: dowolny linux (natywny, bądź zwirtualizowany w virtualbox'ie).

1. "Konsola":
wejście: stdin Modyfikacja/wyjscie: x:=x+1
2. "Argument" Wejście: argument do programu, parsowana za pomocą getopt Modyfikacja/wyjscie: x:= lustrzane odbicie bitów, np dla 6(110):= 3(011)
3. "Pipe" Wejście: Nazwany pipe Modyfikacja/wyjscie: Nastepna liczba pierwsza
4. "Jajko" Wejście: interfejs znakowy (linux kernel module over chrdev) Modyfikacja/wyjscie: Liczba ciagu Fibonacciego odpowiadajaca otrzymanemu numerowi
Wszystkie programy powinny być budowane za pomocą makefile'ow (np. autotools, cmake bądź gnu make). Kod pod kontrolą lokalnego git-a. Sposób uruchamiania dowolny ale zintegrowany (przy użyciu jednej komendy). W przypadku gdy wynik którejś z modyfikacji przekroczy zakres u32, powinna pojawić się o tym informacja na wyjściu. Wejściowa aplikacja „Konsola” przyjmuje liczby do momentu gdy użytkownik wciśnie ctrl+c, wówczas wszystkie programy powinny zostać pozamykane.

## 		Instalacja i uruchomienie

+ Pobieramy repozytorium: *https://gitlab.com/goro3/gluchy_tel.git*
+ W repozytorium jest gotowy plik Makefile przygotowany do instalacji: *make all*
+ Po kompilacji uruchamiamy program przez polecenie ./start
+ Kiedy skończysz można użyć *make clean* do usunięcia programu i posprzątaniu po kompilacji. 

*!!!HAVE FUN!!!*

## 		Credits

Grzegorz Rezmer(goro3), Waldemar Barczyk, Michał Kozłowski, Diana Kalinina
