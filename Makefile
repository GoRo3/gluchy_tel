FLAG = -Wall -Wextra

MAKEDIR = build
SRCDIR = src/

obj-m+=$(SRCDIR)004.o

RMDIR = rm -fr

EXEC = prog_2 prog_3

INSTALL = install -C
INSTALLPROG = $(INSTALL) -m 755
INSTALLMKDIR = $(INSTALL) -d -m 755

.PHONY: install

install: compile
	@$(INSTALLMKDIR) -d $(MAKEDIR)/ && $(INSTALLPROG) $(EXEC) $(MAKEDIR)/
	@$(RMDIR) $(EXEC)

compile: $(EXEC) prog_4 start

start: $(SRCDIR)001.c
	@echo "\n******************** Kompiluje pierwszy program *********************\n"
	$(CC) $(FLAG) -o $@ $?

prog_2: $(SRCDIR)002.c
	@echo "\n******************** Kompiluje drugi program *********************\n"
	$(CC) $(FLAG) -o $@ $?

prog_3: $(SRCDIR)003.c
	@echo "\n******************** Kompiluje trzeci program *********************\n"
	$(CC) $(FLAG) -o $@ $?

.PHONY: clean
clean:
	clear

	@echo "\n******************** Czyszcze repozytorium *********************\n"
	$(RMDIR) $(MAKEDIR)
	$(RMDIR) ./start
	@echo "\n******************* Czyszczenie  modulu z jadra Linux ************************\n"

	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

	@echo "\n******************* Usuawnie modulu z jadra Linux ************************\n"

	sudo rmmod 004.ko

prog_4:

	@echo "\n******************* Kompilowanie  modulu  jadra Linux ************************\n"

	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) modules

	@echo "\n******************* Dodawdanie modulu do jadra Linux ************************\n"

	sudo insmod $(SRCDIR)004.ko

	@echo "\n******************* Dodawanie praw do pliku /dev/liczbaFib ************************\n"

	sudo chmod 666 /dev/liczbaFib

	@echo "\n******************* Koniec dziekuje!  ************************\n"
